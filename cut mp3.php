<?php
//scansiono directory per nome file
$dir = '.';
$name = scandir($dir);
$file_name = "";
for($i = 0; $i< count($name); $i++){
	$file_name = $name[$i];
	preg_match('/.*\.json/', $file_name, $matches, PREG_OFFSET_CAPTURE);
	if (!empty($matches)) {
		//nome file principale
		$file_name = $matches[0][0];
		$file_name = substr_replace($file_name, "", -1);
		$file_name = substr_replace($file_name, "", -1);
		$file_name = substr_replace($file_name, "", -1);
		$file_name = substr_replace($file_name, "", -1);
		$file_name = substr_replace($file_name, "", -1);
		$file_name = substr_replace($file_name, "", -1);
		$file_name = substr_replace($file_name, "", -1);
		$file_name = substr_replace($file_name, "", -1);
		$file_name = substr_replace($file_name, "", -1);
		$file_name = substr_replace($file_name, "", -1);
		break;
	}
}
//creazione nome file JSON e MP3
$name = $file_name;
$nameMP3 = $name . ".mp3";
$nameJson = $name . ".info.json";
//lettura file JSON
$file = file_get_contents($nameJson);
$content = json_decode($file);
if (JSON_ERROR_NONE !== json_last_error()) {
    printf("%s", json_last_error_msg());
    exit;
}
// cut file originale in capitoli
$chapters = $content -> chapters;
for ($i = 0; $i < count($chapters); $i++) {
	$title = $chapters[$i] -> title;
	$title = $title . ".mp3";
	//rimozione caratteri non ammessi in nome file
	$title = str_replace("\\", "", $title);
	$title = str_replace("/", "", $title);
	$title = str_replace(":", "-", $title);
	$title = str_replace("*", "", $title);
	$title = str_replace("?", "", $title);
	$title = str_replace("\"", "'", $title);
	$title = str_replace("<", "(", $title);
	$title = str_replace(">", ")", $title);
	$title = str_replace("|", " ", $title);
	$start = $chapters[$i] -> start_time;
	$end = $chapters[$i] -> end_time;
	$command = "ffmpeg.exe -i " . '"' . $nameMP3 . '"' . " -ss " . $start . " -to " . $end . ' "' . $title . '"';
	echo "\n\n\n\n\nESEGUITI " . ($i+1) . " su " . count($chapters) . "\n\n";
	exec($command);
}
?>